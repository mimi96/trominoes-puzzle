package mvc;

public class Controller
{    
    private View myView;
    private Model myModel;
    
    /**
     * Controller constructor; view must be passed in since 
     * controller has responsibility to notify view when 
     * some event takes place.
     * 
     * The constructor creates a view and model. 
     * 
     * While the board size is not set (determined in Model.java) we will 
     * keep asking the user for a board size. Once a proper board size has
     * been given we will load the display. 
     */
    
    public Controller()
    {
    	myModel = new Model(4,2,2);
    	myView = new View(this);
    }
    
    
    public void solve()
    {
    	myModel.getSolvedBoard();
    }
    
    
        
      
}