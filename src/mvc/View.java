
package mvc;

import java.awt.*;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class View extends JFrame
{
	
	public JFrame frame = new JFrame("Trominoes Puzzle - Bailey and Mimi");
	public JLabel text = new JLabel ("Trominoes Puzzle CSCI 221", JLabel.CENTER);
	
	public Can[][] squares;
	private Controller myController;
	
	
	public View(Controller controller)
	{
		//frame components 
				frame.setSize(1000,1000);
				frame.setLocationRelativeTo(null);
				frame.setLayout(new FlowLayout());
				frame.getContentPane().setBackground(Color.darkGray);
				
				JPanel board = new JPanel();
				board.setLayout(new GridLayout(6,6));
				board.setSize(500,500);
				board.setBackground(Color.white);
				
				//text components
				text.setFont(new Font("Serif", Font.BOLD,40));
				text.setForeground(Color.red);
				
				
				
				
				//add to frame
				frame.add(text);
				frame.add(board);
				frame.setVisible(true);
		
	}

}